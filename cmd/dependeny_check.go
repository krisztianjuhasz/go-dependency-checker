package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/interticket/it-stemx/media/application"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "depcheck",
	Short: "Dependency check checks your imports version",
	Run: func(cmd *cobra.Command, args []string) {
		application.DependencyCheck()
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println("error while doing things", err)
		os.Exit(1)
	}
}
