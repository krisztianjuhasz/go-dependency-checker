package application

import (
	"bufio"
	"encoding/json"
	"fmt"
	"gitlab.com/interticket/it-stemx/media/domain"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	gomodFileName = "go.mod"
	githubDomain = "github.com"
)

func DependencyCheck() {
	imports := readDependenciesFromFile(gomodFileName)
	for _, s := range imports {
		parts := strings.Split(s, " ")
		releases := getReleases(parts[0])
		for _, release := range releases {
			if release.TagName != parts[1] {
				fmt.Println(parts[0])
				fmt.Println(release.Name)
				fmt.Println(release.Body)
			}
			if release.TagName == parts[1] {
				break
			}
		}
	}
}

func getReleases(repo string) domain.GithubReleases {
	resp, err := http.Get("https://api.pkg.go.dev/github.com/gin-gonic/gin?tab=versions")
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	println(string(body))


	if strings.Contains(repo, githubDomain) {
		repo = strings.TrimPrefix(repo, githubDomain)
		//url := "https://api.github.com/repos/googleapis/google-cloud-go/releases?per_page=100"
		url := "https://api.github.com/repos" + repo + "/releases"
		resp, err := http.Get(url)
		if err != nil {
			fmt.Println(err)
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err)
		}
		releases := domain.GithubReleases{}
		err = json.Unmarshal(body, &releases)
		if err != nil {
			fmt.Println(err)
		}
		return releases
	}
	return nil
}

func readDependenciesFromFile(name string) []string {
	file, err := os.Open(name)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	inRequire := false
	imports := make([]string, 0)
	for scanner.Scan() {
		text := scanner.Text()
		if text == "require (" {
			inRequire = true
		}
		if text == ")" {
			inRequire = false
		}
		if strings.HasPrefix(text, "require") && !strings.HasPrefix(text, "require (") && !strings.HasSuffix(text, "// indirect") {
			imports = append(imports, strings.TrimSpace(strings.TrimPrefix(text, "require ")))
		}
		if text != "require (" && inRequire && !strings.HasSuffix(text, "// indirect") {
			imports = append(imports, strings.TrimSpace(text))
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return imports
}

func check(e error) {
	if e != nil {
		fmt.Println(e)
		panic(e)
	}
}
