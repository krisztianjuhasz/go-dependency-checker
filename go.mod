module gitlab.com/interticket/it-stemx/media

require (
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
)

go 1.15
