package main

import (
	"gitlab.com/interticket/it-stemx/media/cmd"
)

func main() {
	cmd.Execute()
}
