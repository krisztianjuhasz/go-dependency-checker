package domain

type GithubReleases []GithubRelease

type GithubRelease struct {
	Url     string `json:"url"`
	Name    string `json:"name"`
	ID      int    `json:"id"`
	Body    string `json:"body"`
	TagName string `json:"tag_name"`
}
